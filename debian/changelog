vcftools (0.1.16-3) unstable; urgency=medium

  * Fix watchfile to detect new versions on github (routine-update)
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 07 Oct 2021 07:36:06 +0200

vcftools (0.1.16-2) unstable; urgency=medium

  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Remove trailing whitespace in debian/control (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 16 Nov 2020 18:47:17 +0100

vcftools (0.1.16-1) unstable; urgency=medium

  * New upstream version dealing with CVE-2018-11099, CVE-2018-11129 and
    CVE-2018-11130
    Closes: #902190
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5
  * Remove unneeded get-orig-source target
  * Fix interpreter line in Perl scripts

 -- Andreas Tille <tille@debian.org>  Fri, 03 Aug 2018 16:20:50 +0200

vcftools (0.1.15-1) unstable; urgency=medium

  * New upstream version

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - added references to registries
    - yamllint cleanliness

  [ Andreas Tille ]
  * Standards-Version: 4.1.3
  * debhelper 11
  * d/rules: do not parse d/changelog

 -- Andreas Tille <tille@debian.org>  Sun, 18 Feb 2018 20:08:00 +0100

vcftools (0.1.14+dfsg-4) unstable; urgency=medium

  * make -p work without . on @INC (Thanks for the patch to Niko Tyni
    <ntyni@debian.org>)
    Closes: #842794
  * debhelper 10
  * d/watch: version=4
  * add test script to /usr/share/doc/vcftools to enable users reproducing
    the test

 -- Andreas Tille <tille@debian.org>  Thu, 15 Dec 2016 22:56:59 +0100

vcftools (0.1.14+dfsg-3) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Move packaging from SVN to Git
  * cme fix dpkg-control
  * hardening=+all
  * fix some spelling
  * add missing manpage

  [ Canberk Koç ]
  * autopkgtest added
  * fix for broken tests

 -- Canberk Koç <canberkkoc@gmail.com>  Sun, 03 Jul 2016 13:32:34 +0300

vcftools (0.1.14+dfsg-2) unstable; urgency=medium

  * Set Perl module dir in configure option
    Closes: #806708

 -- Andreas Tille <tille@debian.org>  Mon, 30 Nov 2015 16:41:36 +0100

vcftools (0.1.14+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Source has moved to github
    Closes: #805287
  * Remove unneeded get-orig-source script
  * Adapt to new build system
  * Delete unneeded overrides
  * Remove redundant README.source
  * move manpages into common dir

 -- Andreas Tille <tille@debian.org>  Mon, 16 Nov 2015 15:25:49 +0100

vcftools (0.1.13+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Adapt to new uscan
  * cme fix dpkg-control
  * Priority: optional
  * Fix permission of example files
  * DEP5 fix

 -- Andreas Tille <tille@debian.org>  Sun, 09 Aug 2015 08:15:47 +0200

vcftools (0.1.12+dfsg-1) unstable; urgency=low

  * new upstream version
  * debian/control: standard bumped to 3.9.5 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 04 Apr 2014 18:00:00 +0200

vcftools (0.1.11+dfsg-1) unstable; urgency=low

  * new upstream version
  * debian/patches/use-dpkg-buildflags.patch: adatpt for new version
  * debian/control: adapt VCS*-lines according to lintian

 -- Thorsten Alteholz <debian@alteholz.de>  Fri, 28 Jun 2013 18:00:00 +0200

vcftools (0.1.10+dfsg-1) unstable; urgency=low

  [ Thorsten Alteholz ]
  * add man pages for new scripts
  * debian/patches/use-dpkg-buildflags.patch: add LDFLAGS to link stage
  * debian/vcftools.lintian-overrides: false positive
  * debian/control: standard bumped to 3.9.4 (no changes)
  * debian/control: DM-Upload-Allowed removed

  [ Dominique Belhachemi ]
  * New upstream version
  * Updated debian/patches/use-dpkg-buildflags.patch
  * Removed debian/patches/perl.patch (applied upstream)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 24 Feb 2013 16:00:00 +0100

vcftools (0.1.9+dfsg-2) unstable; urgency=low

  * debian/control: description changed (Closes: #689058)
  * debian/patches/perl.patch for new syntax (Closes: #689059)
  * fix get-orig-source to handle dfsg

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 02 Dec 2012 19:00:00 +0100

vcftools (0.1.9+dfsg-1) UNRELEASED; urgency=low

  * debian/copyright:
     - DEP5
     - Add Files-Excluded to document what was removed from original source
  * debian/watch: Enable handling dfsg suffix

 -- Andreas Tille <tille@debian.org>  Wed, 12 Sep 2012 16:31:09 +0200

vcftools (0.1.9-1) unstable; urgency=low

  * New upstream version
  * debian/upstream: Added citations
  * debian/control:Standards-Version: 3.9.3 (no changes needed)
  * debian/{get-orig-source,rules,watch}: Make sure we always get the
    latest version
  * debhelper 9 (control+compat)
  * debian/{rules,install,examples}: use debhelper files to install
    files (upstream changed some targets so the old copying mechanism
    did not worked any more)
  * debian/patches/use-dpkg-buildflags.patch: Enable propagation of
    hardening flags

 -- Andreas Tille <tille@debian.org>  Sat, 12 May 2012 09:31:58 +0200

vcftools (0.1.7-1) unstable; urgency=low

  * new upstream version
  * rearrange debian/copyright to calm lintian

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 19 Oct 2011 18:00:00 +0200

vcftools (0.1.6-1) unstable; urgency=low

  [ Thorsten ]
  * Initial release (Closes: #633142).

  [ Steffen as the package's sponsor ]
  * Added README.source
  * Removed Andreas and myself from uploaders
  * Followed FTPmaster's advice to remove the source-less .pdf from source tree

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 10 Jul 2011 15:18:25 +0200
